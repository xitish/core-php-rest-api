<?php

namespace Objects;

use database\Database;

class BaseObject
{
    public $connection;

    public function __construct()
    {
        $database = new Database();
        $this->connection = $database->getConnection();
    }
}