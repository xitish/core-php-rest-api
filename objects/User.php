<?php

require_once('..\database\Database.php');

class User
{
    private $connection;
    private $table = "users";

    public $id;
    public $name;
    public $email;
    public $password;
    /**
     * Client constructor.
     */
    public function __construct()
    {
        $database = (new Database())->getConnection();
        $this->connection = $database;
    }

    /**
     * Create a User
     *
     * @return bool
     */
    public function create()
    {
        $query = "INSERT INTO " . $this->table . "
            SET
                name = :name,
                email = :email,
                password = :password";

        $stmt = $this->connection->prepare($query);

        // Sanitize Input
        $this->name=htmlspecialchars(strip_tags($this->name));
        $this->email=htmlspecialchars(strip_tags($this->email));
        $this->password=htmlspecialchars(strip_tags($this->password));

        $stmt->bindParam(':name', $this->name);
        $stmt->bindParam(':email', $this->email);
        $password = password_hash($this->password, PASSWORD_BCRYPT);
        $stmt->bindParam(':password', $password);

        if($stmt->execute()){
            return true;
        }
        return false;
    }

    /**
     * Check if the email supplied by user on login exists
     *
     * @return bool
     */
    public function emailExists()
    {
        $query = "SELECT id, name, name, password
            FROM " . $this->table . "
            WHERE email = ?
            LIMIT 0,1";

        $stmt = $this->connection->prepare( $query );
        $this->email=htmlspecialchars(strip_tags($this->email));
        $stmt->bindParam(1, $this->email);
        $stmt->execute();

        if($stmt->rowCount() > 0)
        {
            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            $this->id = $row['id'];
            $this->name = $row['name'];
            $this->password = $row['password'];

            return true;
        }
        return false;
    }

}