<?php

    require_once('..\..\database\Database.php');

    class Client
    {
        private $connection;
        private $table = "clients";

        public $id;
        public $first_name;
        public $last_name;
        public $dob;
        public $email;
        public $phone;

        /**
         * Client constructor.
         */
        public function __construct()
        {
            $database = (new Database())->getConnection();
            $this->connection = $database;
        }

        /**
         * Client Index: Show the List of Clients (50 Clients will be returned by default)
         *
         * @param int $limit
         * @return bool|PDOStatement
         */
        public function index($limit = 50)
        {
            // Query
            $query = "SELECT * FROM " . $this->table . " LIMIT " . $limit;
            $stmt = $this->connection->prepare($query);
            $stmt->execute();

            return $stmt;
        }

        /**
         * Show Info of a single User
         *
         * @param $id
         * @return bool|PDOStatement
         */
        public function show($id)
        {
            $query = "SELECT * FROM " . $this->table . " WHERE id = ? LIMIT 1";
            $stmt = $this->connection->prepare($query);
            $stmt->bindParam(1, $id);
            $stmt->execute();

            return $stmt->fetch(PDO::FETCH_ASSOC);
        }

        /**
         * Save User Info to DB
         *
         * @return mixed
         */
        public function store()
        {
            // Sanitize Input
            $this->first_name=htmlspecialchars(strip_tags($this->first_name));
            $this->last_name=htmlspecialchars(strip_tags($this->last_name));
            $this->dob=htmlspecialchars(strip_tags($this->dob));
            $this->email=htmlspecialchars(strip_tags($this->email));
            $this->phone=htmlspecialchars(strip_tags($this->phone));

            // Query
            $query = "INSERT INTO
                " . $this->table . "
            SET
                first_name=:first_name,
                last_name=:last_name, 
                dob=:dob,
                email=:email,
                phone=:phone";

            $stmt = $this->connection->prepare($query);

            // Bind Parameters
            $stmt->bindParam(":first_name", $this->first_name);
            $stmt->bindParam(":last_name", $this->last_name);
            $stmt->bindParam(":dob", $this->dob);
            $stmt->bindParam(":email", $this->email);
            $stmt->bindParam(":phone", $this->phone);

            if($stmt->execute()){
                return true;
            }
            return false;
        }
    }