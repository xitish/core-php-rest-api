<?php

header("Access-Control-Allow-Origin: http://localhost/rest-api-authentication-example/");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include_once '../lib/BeforeValidException.php';
include_once '../lib/ExpiredException.php';
include_once '../lib/SignatureInvalidException.php';
include_once '../lib/JWT.php';
use \Firebase\JWT\JWT;

require_once('../objects/User.php');
require_once('../lib/core.php');


$data = json_decode(file_get_contents("php://input"));
$user = new User();

if(!empty($data->email) && !empty($data->password) ) {
    $user->email = $data->email ?? '';

    // If Email and Password Match
    if ($user->emailExists() && password_verify($data->password, $user->password)) {
        $token = array(
            "iss" => $iss,
            "aud" => $aud,
            "iat" => $iat,
            "nbf" => $nbf,
            "data" => array(
                "id" => $user->id,
                "name" => $user->name,
                "email" => $user->email
            )
        );

        http_response_code(200);

        $jwt = JWT::encode($token, $key);
        $response = [
            'status' => 'success',
            "message" => "Successful login.",
            "token" => $jwt
        ];
    } else {
        http_response_code(401);
        $response = [
            'status' => 'unsuccessful',
            'msg' => 'Login Failed. Invalid Email or Password'
        ];
    }
}else{
    http_response_code(401);
    $response = [
        'status' => 'unsuccessful',
        'msg' => 'Provide all required Information',
        'required_info' => [
            'email', 'password'
        ]
    ];
}

echo json_encode($response);



