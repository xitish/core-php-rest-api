<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

require_once('../objects/User.php');

$user = new User();

$data = json_decode(file_get_contents("php://input"));

$user->name = $data->name ?? '';
$user->email = $data->email ?? '';
$user->password = $data->password ?? '';

if(
    !empty($user->name) &&
    !empty($user->email) &&
    !empty($user->password)
){
    if($user->create()){
        http_response_code(200);

        $response = [
            'status' => 'success',
            'msg' => "User was created. Login to Continue"
        ];
    }
    else{
        http_response_code(503);
        $response = [
            'status' => 'unsuccessful',
            'msg' => 'Failed in Creating User'
        ];
    }
}
else{
    http_response_code(400);
    $response = [
        'status' => 'failed',
        'msg' => 'Provide all Data',
        'required_data' => [
            'name', 'email', 'password'
        ]
    ];
}
echo json_encode($response);





