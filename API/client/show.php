<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

include_once '../../lib/BeforeValidException.php';
include_once '../../lib/ExpiredException.php';
include_once '../../lib/SignatureInvalidException.php';
include_once '../../lib/JWT.php';
use \Firebase\JWT\JWT;

require_once('../../objects\Client.php');
require_once('../../lib/core.php');

$response['meta'] = [];
$response['data'] = [];

if(array_key_exists('token', $_GET)) {
    $token = $_GET['token'];

    // Try to Decode the Token
    try {
        $decoded = JWT::decode($token, $key, array('HS256'));

        // If a id is provided
        if(isset($_GET['id'])){
            $clientId = $_GET['id'];
            $clientOb = new Client();
            $client = $clientOb->show($clientId);

            // If a client with given ID Exists
            if($client) {
                $response['data'] = [
                    'id' => $client['id'],
                    'name' => $client['first_name'] . ' ' . $client['last_name'],
                    'date_of_birth' => $client['dob'],
                    'contact' => [
                        'email' => $client['email'],
                        'phone' => $client['phone'],
                    ],
                ];

                $response['meta'] = [
                    'status' => 'success'
                ];
            }

            // Client Not found
            else{
                http_response_code(404);
                $response['meta'] = [
                    'status' => 'not_found',
                    'msg' => 'No Client with given ID found.'
                ];
            }
        }
        else{
            http_response_code(400);
            $response = [
                'status' => 'error',
                'msg' => 'Please Provide a User Id.'
            ];
        }
    }
    // If token is Invalid
    catch (Exception $e){
        http_response_code(401);
        $response = [
            'status' => 'failed',
            'msg' => 'Invalid Token or token expired.',
            'error' => $e->getMessage()
        ];
    }
}

// No token in the request
else{
    http_response_code(401);
    $response = [
        'status' => 'failed',
        'msg' => 'Not Authorized to Access the resource.',
        'error' => 'No token Provided.'
    ];
}

echo json_encode($response);

