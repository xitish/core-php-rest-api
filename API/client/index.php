<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

// Improve

include_once '../../lib/BeforeValidException.php';
include_once '../../lib/ExpiredException.php';
include_once '../../lib/SignatureInvalidException.php';
include_once '../../lib/JWT.php';
use \Firebase\JWT\JWT;

require_once('../../objects\Client.php');
require_once('../../lib/core.php');

// If Token is included in the request
if(array_key_exists('token', $_GET)) {
    $token = $_GET['token'];

    // Try to Decode the Token
    try {
        $decoded = JWT::decode($token, $key, array('HS256'));

        // Set Response Code: 200 OK
        http_response_code(200);

        $clientOb = new Client();
        $result = $clientOb->index(10);

        // Prepare the response to be sent
        $response['meta'] = [];
        $response['data'] = [];

        // Set Meta Info
        $response['meta'] = [
            'status' => 'success',
            'count' => $result->rowCount(),
        ];

        while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
            $client = [
                'name' => $row['first_name'] . ' ' . $row['last_name'],
                'date_of_birth' => $row['dob'],
                'contact' => [
                    'email' => $row['email'],
                    'phone' => $row['phone'],
                ],
            ];

            array_push($response['data'], $client);
        }
    }

    // If token is Invalid
    catch (Exception $e){
        http_response_code(401);
        $response = [
            'status' => 'failed',
            'msg' => 'Invalid Token or token expired.',
            'error' => $e->getMessage()
        ];
    }

}
// No token in the request
else{
    http_response_code(401);
    $response = [
        'status' => 'failed',
        'msg' => 'Not Authorized to Access the resource.',
        'error' => 'No token Provided.'
    ];
}
echo json_encode($response);
