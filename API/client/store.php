<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

include_once '../../lib/BeforeValidException.php';
include_once '../../lib/ExpiredException.php';
include_once '../../lib/SignatureInvalidException.php';
include_once '../../lib/JWT.php';
use \Firebase\JWT\JWT;

require_once('../../objects\Client.php');
require_once('../../lib/core.php');

if(array_key_exists('token', $_GET)) {
    $token = $_GET['token'];

    // Try to Decode the Token
    try {
        $decoded = JWT::decode($token, $key, array('HS256'));

        $data = json_decode(file_get_contents("php://input"));

        // Validate Data
        if (
            !empty($data->first_name) &&
            !empty($data->last_name) &&
            !empty($data->dob) &&
            !empty($data->email) &&
            !empty($data->phone)
        ) {
            $client = new Client();

            $client->first_name = $data->first_name;
            $client->last_name = $data->last_name;
            $client->dob = $data->dob;
            $client->email = $data->email;
            $client->phone = $data->phone;

            if ($client->store()) {
                http_response_code(201);
                $response['meta'] = [
                    'status' => 'success',
                    'msg' => 'Client Created',
                ];

                $response['data'] = [
                    'client' => [
                        'id' => 'Sample Client ID'
                    ]
                ];
            } else {
                http_response_code(503);
                $response['meta'] = [
                    'status' => 'unsuccessful',
                    'msg' => 'Could Not Create Client'
                ];
            }
        } // Invalid Data
        else {
            http_response_code(400);
            $response = [
                'status' => 'error',
                'msg' => 'Provide all Details',
                'required_details' => [
                    'first_name', 'last_name', 'dob', 'email', 'phone'
                ]
            ];
        }
    } // If token is Invalid
    catch (Exception $e) {
        http_response_code(401);
        $response = [
            'status' => 'failed',
            'msg' => 'Invalid Token or token expired.',
            'error' => $e->getMessage()
        ];
    }
}
// No token in the request
else{
    http_response_code(401);
    $response = [
        'status' => 'failed',
        'msg' => 'Not Authorized to Access the resource.',
        'error' => 'No token Provided.'
    ];
}

echo json_encode($response);


