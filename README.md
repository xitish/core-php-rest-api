### Usage
- Send __POST__ request to register endpoint with credentials to create new user.
- After successful registration, send __POST__ request to login endpoint with the credentials.
- A `token` is provided upon successful login.
- Send the `token` in each subsequent requests to access the _Client_ resource.

### Endpoints
- [__GET__] List of Clients: `/api/client/index.php` params: `token`
- [__GET__] One Client Details: `localhost:8000/api/client/show.php` params: `id`, `token`
- [__POST__] Add Client: `localhost:8000/api/client/store.php` params: `token` | data: `first_name`, `last_name`, `dob`, `email`, `password`

> Auth
- [__POST__] Register: `/auth/register.php` data: `name`, `email`, `password`
- [__POST__] Login: `/auth/login.php` data: `email`, `password`

### User Credentials
- `root@gmail.com`:`secret`
- `admin@gmail.com`:`secret`